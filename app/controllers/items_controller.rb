class ItemsController < ApplicationController

  before_action :find_item, only: [:show, :edit, :update, :destroy]

  def index
    # @items = Item.all.order("created_at desc")
    # prevent when others user signed up look at your items
    if user_signed_in?
      @items = Item.where(user_id: current_user.id).order("created_at desc")
    end
  end

  def show
    # same as find_item
    # need before_action
  end

  def new
    @item = current_user.items.build
  end

  def create
    @item = current_user.items.build(item_params)

    if @item.save
      redirect_to root_path
    else
      render 'new'
    end
  end

  def edit
    
  end

  def update
    if @item.update(item_params)
      redirect_to item_path(@item) # show page
    else
      render 'edit'
    end
  end

  def destroy
    @item.destroy
    redirect_to root_path
  end

  def complete
    find_item
    @item.update_attribute(:completed_at, Time.now)
    redirect_to root_path
  end

  private

    def find_item
      @item = Item.find(params[:id])
    end

    def item_params
      params.require(:item).permit(:title, :description)
    end
end